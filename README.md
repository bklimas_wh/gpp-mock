Start application

Gpp-mock configuration MUBE:

```
#local mock
hexopay:
  url: http://localhost:8100
  notificationUrl: http://localhost:8085/deposit/callback
  payment:
    endpoint: /transactions/payment
  payout:
    endpoint: /transactions/payout
  credentials:
    username: 7359
    password: 15caa6b787cfc72c940b3fd2135a72a2b051248e53709125fe7336083ee6d3a8
```

Gpp-mock configuration Payment-IQ when you want mock all downlines for gpp-flow-orchestrator:
```yaml
gpp-data.url: "http://localhost:8100/ds"
gpp-unity.url: "http://localhost:8100/un"
gpp-scheduler.url: "http://localhost:8100/sh"
gpp-pmb.url: "http://localhost:8100/pmb"
gpp-payment-iq.url: "http://localhost:8100/piq"

gpp-openbet.url: "http://localhost:8084"
gpp-hexopay.url: "http://localhost:8085"

gpp-flow-gateway:
  url:
    com: "http://localhost:8100/fg"
    es: "http://localhost:8080"
    it: "http://localhost:8080"
```