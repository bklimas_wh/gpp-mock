package com.wh.gppmock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GppLocalMockApplication {

	public static void main(String[] args) {
		SpringApplication.run(GppLocalMockApplication.class, args);
	}

}
