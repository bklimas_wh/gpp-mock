package com.wh.gppmock.controlers;

import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.wh.gppmock.domain.ds.PaymentMethodInstanceCollection;
import com.wh.gppmock.domain.ds.ProcessMappingRequest;
import com.wh.gppmock.domain.ds.TransactionLog;
import com.wh.gppmock.service.DataStoreService;
import com.wh.gppmock.service.PaymentMethodInstanceService;
import com.wh.gppmock.service.ProcessMappingService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/ds")
@AllArgsConstructor
public class DataStoreController {
    private final DataStoreService dataStoreService;
    private final ProcessMappingService processMappingService;
    private final PaymentMethodInstanceService paymentMethodInstanceService;

    @PostMapping("/transactionlog")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void create(final  @RequestBody TransactionLog transactionLog) {
        dataStoreService.save(transactionLog);
    }

    @PostMapping("/processMapping")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Long create(final @RequestBody ProcessMappingRequest processMapping) {
        return processMappingService.create(processMapping);
    }

    @GetMapping("/paymentMethodInstance")
    public PaymentMethodInstanceCollection findByAttributes(
        final @RequestParam Map<String, String> attributes) {
        return paymentMethodInstanceService.findByAttributes(attributes);
    }
}
