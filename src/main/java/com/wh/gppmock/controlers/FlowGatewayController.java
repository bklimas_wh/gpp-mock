package com.wh.gppmock.controlers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wh.gppmock.domain.fg.StateResponse;
import com.wh.gppmock.domain.fg.StateUpdate;
import com.wh.gppmock.service.FlowGatewayService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/fg")
@AllArgsConstructor
public class FlowGatewayController {
    private final FlowGatewayService flowGatewayService;

    @PostMapping("/state/{processInstanceId}")
    public void update(@PathVariable final long processInstanceId, @RequestBody final StateUpdate body) {
        flowGatewayService.update(processInstanceId, body);
    }

    @GetMapping("/state/{processInstanceId}/{version}")
    public StateResponse getState(@PathVariable final long processInstanceId, @PathVariable final int version) {
        var state = flowGatewayService.getState(processInstanceId, version);
        return StateResponse.builder()
            .state(state)
            .build();
    }
}
