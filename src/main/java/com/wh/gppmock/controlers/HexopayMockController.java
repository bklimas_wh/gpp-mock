package com.wh.gppmock.controlers;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wh.gppmock.executors.CallbackExecutor;
import com.wh.gppmock.domain.hexopay.MuchBetter;
import com.wh.gppmock.domain.hexopay.Payment;
import com.wh.gppmock.domain.hexopay.PaymentRequest;
import com.wh.gppmock.domain.hexopay.PaymentResponse;
import com.wh.gppmock.domain.hexopay.PaymentResponseTransaction;
import com.wh.gppmock.measure.annotation.Measure;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping("/transactions")
@AllArgsConstructor
public class HexopayMockController {
    private final CallbackExecutor callback;

    @Measure("HP-PAY")
    @PostMapping("/payment")
    public ResponseEntity<PaymentResponse> payment(@RequestBody final PaymentRequest paymentRequest) {

        var paymentResponse = buildPaymentResponse(paymentRequest);

        callback.perform(paymentRequest, paymentResponse);
        log.info("After payment method, TrackingId {}", paymentRequest.getRequest().getTrackingId());
        return ResponseEntity.status(HttpStatus.CREATED).body(paymentResponse);
    }

    private PaymentResponse buildPaymentResponse(PaymentRequest paymentRequest) {
        var paymentResponseTransaction = PaymentResponseTransaction.builder()
            .uid(UUID.randomUUID().toString())
            .type("payment")
            .status("pending")
            .amount(paymentRequest.getRequest().getAmount())
            .currency(paymentRequest.getRequest().getCurrency())
            .description(paymentRequest.getRequest().getDescription())
            .createdAt(LocalDateTime.now().toString())
            .updatedAt(LocalDateTime.now().toString())
            .methodType("much_better")
            .payment(Payment.builder()
                .status("pending")
                .gatewayId(15605)
                .refId("32426297")
                .message("Transaction awaits confirmation")
                .build())
            .muchBetter(MuchBetter.builder()
                .type(paymentRequest.getRequest().getMethod().getType())
                .account(paymentRequest.getRequest().getMethod().getAccount())
                .channel(paymentRequest.getRequest().getMethod().getChannel())
                .customerStatus("registered")
                .build())
            .message("Transaction awaits confirmation")
            .trackingId(paymentRequest.getRequest().getTrackingId())
            .test(false)
            .build();

        return PaymentResponse.builder()
            .transaction(paymentResponseTransaction)
            .build();
    }

}
//<201,PaymentResponse(transaction=PaymentResponseTransaction
// (uid=6f2b9779-6ba4-40ee-8d65-d7507c364f35,
// type=payment, status=pending,
// amount=200, currency=EUR,
// description=WH Transaction reference: 948266,
// createdAt=2020-10-02T07:54:01Z,
// updatedAt=2020-10-02T07:54:01Z,
// methodType=much_better,
// payment=Payment(status=pending, gatewayId=15605, refId=32426297, message=Transaction awaits confirmation),
// muchBetter=MuchBetter(type=much_better, account=48698657087, channel=1|Logo Text, customerStatus=registered),
// message=Transaction awaits confirmation,
// trackingId=9d95126f-5f57-4496-a892-e453f37ad44c,
// test=false, language=null,
// billingAddress=null,
// additionalData=AdditionalData(paymentMethod=PaymentMethod(type=alternative)),
// beProtectedVerification=BeProtectedVerification(whiteBlackList=WhiteBlackList(email=absent, ip=absent),
// rules={31_4835_William Hill={}, 31_Elad Rotzak={}, Hexopay={}}))