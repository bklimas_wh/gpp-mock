package com.wh.gppmock.controlers;

import java.util.Collection;
import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wh.gppmock.domain.pmb.AccountLimitCollection;
import com.wh.gppmock.domain.pmb.MethodLimit;
import com.wh.gppmock.domain.pmb.PaymentMethodTransactionRequest;
import com.wh.gppmock.domain.pmb.RequestMode;
import com.wh.gppmock.domain.pmb.ValidationRequest;
import com.wh.gppmock.domain.pmb.ValidationResult;
import com.wh.gppmock.service.PMBService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/pmb")
@AllArgsConstructor
public class PMBController {
    private final PMBService service;

    @GetMapping("/paymentMethodBalance/limits/{customerId}")
    public Collection<MethodLimit> getPaymentMethodBalanceLimits(@PathVariable final UUID customerId) {
        return service.getPaymentMethodBalanceLimits(customerId);
    }

    @PostMapping("/paymentMethodTransaction")
    public void processPaymentMethodTransaction(@RequestBody final PaymentMethodTransactionRequest transaction) {
        var transactionWithMode = transaction.toBuilder().requestMode(RequestMode.ADD).build();
        service.processPaymentMethodTransaction(transactionWithMode);
    }

    @GetMapping("/paymentMethodBalance/account/limits/{customerId}")
    public AccountLimitCollection getAccountLimits(@PathVariable final UUID customerId) {
        return service.getAccountLimits(customerId);
    }

    @PostMapping("/paymentMethodBalance/validate")
    public ValidationResult validateAgainstPaymentMethodBalance(@RequestBody final ValidationRequest request) {
        return service.validateAgainstPaymentMethodBalance(request);
    }
}
