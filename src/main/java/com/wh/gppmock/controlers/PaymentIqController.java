package com.wh.gppmock.controlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.wh.gppmock.domain.piq.CallbackResult;
import com.wh.gppmock.service.PaymentIqService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/piq")
@AllArgsConstructor
public class PaymentIqController {
    private final PaymentIqService paymentIqService;

    @PostMapping("/callbackresult/{callbackId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void processCallbackResult(@PathVariable final String callbackId,
                                      @RequestBody final CallbackResult callbackResult) {
        paymentIqService.processCallbackResult(callbackId, callbackResult);
    }
}
