package com.wh.gppmock.controlers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wh.gppmock.domain.sh.TimerRequest;
import com.wh.gppmock.service.TimerService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sh/timer")
@AllArgsConstructor
public class SchedulerController {
    private TimerService timerService;

    @PostMapping
    public void addTimer(@RequestBody final TimerRequest timerRequest) {
        timerService.addTimer(timerRequest.getContainerId(), timerRequest.getProcessId(), timerRequest.getSignalName(),
            timerRequest.getSubType());
    }

    @DeleteMapping
    public void deleteTimer(@RequestParam final String containerId,
                            @RequestParam final String processId,
                            @RequestParam final String signalName) {
        timerService.deleteTimer(containerId, processId, signalName);
    }
}
