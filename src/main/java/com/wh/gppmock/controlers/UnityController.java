package com.wh.gppmock.controlers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wh.gppmock.domain.un.AuthoriseUnityRequest;
import com.wh.gppmock.domain.un.CompleteUnityRequest;
import com.wh.gppmock.domain.un.StartUnityRequest;
import com.wh.gppmock.domain.un.StartUnityResponse;
import com.wh.gppmock.domain.un.UnityResponse;
import com.wh.gppmock.service.UnityService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/un")
@AllArgsConstructor
public class UnityController {
    private final UnityService unityService;

    @PostMapping("/deposit/start")
    public StartUnityResponse startDeposit(final @RequestBody StartUnityRequest startUnityRequest) {
        return unityService.startDeposit(startUnityRequest);
    }

    @PostMapping("/withdrawal/start")
    public StartUnityResponse startWithdrawal(final @RequestBody StartUnityRequest startUnityRequest) {
        return unityService.startWithdrawal(startUnityRequest);
    }

    @PostMapping("/deposit/authorise")
    public void authoriseDeposit(final @RequestBody AuthoriseUnityRequest authoriseUnityRequest) {
        unityService.authoriseDeposit(authoriseUnityRequest);
    }

    @PostMapping("/withdrawal/authorise")
    public UnityResponse authoriseWithdrawal(final @RequestBody AuthoriseUnityRequest authoriseUnityRequest) {
        return unityService.authoriseWithdrawal(authoriseUnityRequest);
    }

    @PostMapping("/deposit/complete")
    public void completeDeposit(final @RequestBody CompleteUnityRequest completeUnityRequest) {
        unityService.completeDeposit(completeUnityRequest);
    }

    @PostMapping("/withdrawal/complete")
    public UnityResponse completeWithdrawal(final @RequestBody CompleteUnityRequest completeUnityRequest) {
        return unityService.completeWithdrawal(completeUnityRequest);
    }

//
//    @PostMapping("complete")
//    public UnityResponse complete(final @RequestBody CompleteUnityRequest completeUnityRequest) {
//        return depositService.complete(completeUnityRequest);
//    }
//
//    @PostMapping("fail")
//    public void fail(final @RequestBody FailUnityRequest failUnityRequest) {
//        depositService.fail(failUnityRequest);
//    }
}
