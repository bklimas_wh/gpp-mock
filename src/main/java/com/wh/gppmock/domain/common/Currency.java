package com.wh.gppmock.domain.common;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Currency implements Serializable {
    JPY("¥", 0),
    GBP("£", 2),
    EUR("€", 2),
    CHF("CHF", 2),
    USD(Constants.DOLLAR_SYMBOL, 2),
    CAD(Constants.DOLLAR_SYMBOL, 2),
    AUD(Constants.DOLLAR_SYMBOL, 2),
    SGD(Constants.DOLLAR_SYMBOL, 2);

    private static final String CODE_FIELD = "code";

    @JsonProperty
    private final String symbol;
    @JsonProperty
    private final int decimalPlaces;

    public static Currency create(final String code) {
        return Currency.valueOf(code);
    }

    @JsonCreator
    public static Currency create(final JsonNode jsonNode) {
        var code = jsonNode.isObject() ? jsonNode.get(CODE_FIELD).asText() : jsonNode.asText();
        return Currency.valueOf(code);
    }

    @JsonProperty
    public String getCode() {
        return this.name();
    }

    public int convertToMinimalUnits(final String amount) {
        var bigDecimalAmount = roundIfNecessary(new BigDecimal(amount.trim()).stripTrailingZeros());

        if (bigDecimalAmount.scale() > decimalPlaces) {
            throw new NumberFormatException(
                String.format(
                    "Currency %s can't have %d decimal places!",
                    this.getCode(),
                    bigDecimalAmount.scale()
                )
            );
        }

        return bigDecimalAmount.movePointRight(decimalPlaces).intValue();
    }

    private BigDecimal roundIfNecessary(final BigDecimal bigDecimalAmount) {
        return (this == JPY) ? bigDecimalAmount.setScale(0, RoundingMode.DOWN) : bigDecimalAmount;
    }

    private static class Constants {
        public static final String DOLLAR_SYMBOL = "$";
    }
}
