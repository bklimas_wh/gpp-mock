package com.wh.gppmock.domain.common;

public enum Jurisdiction {
    COM,
    ES,
    IT
}
