package com.wh.gppmock.domain.common;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Money {
    Integer value;
    Currency currency;
}
