package com.wh.gppmock.domain.ds;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class PaymentMethodInstance implements Serializable {
    UUID cpmId;
    UUID customerId;
    PaymentMethodType type;
    PaymentMethodStatus status;
    Map<String, String> attributes;
}
