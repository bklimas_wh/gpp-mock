package com.wh.gppmock.domain.ds;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class PaymentMethodInstanceCollection {
    Integer currentPage;
    Integer totalPages;
    Collection<PaymentMethodInstance> paymentMethodInstances;
}
