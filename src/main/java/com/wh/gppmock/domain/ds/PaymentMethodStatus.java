package com.wh.gppmock.domain.ds;

public enum PaymentMethodStatus {
    ENABLED, DISABLED, DELETED
}