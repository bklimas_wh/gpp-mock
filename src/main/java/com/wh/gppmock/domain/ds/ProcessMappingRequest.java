package com.wh.gppmock.domain.ds;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class ProcessMappingRequest {
    Long processInstanceId;
    UUID transactionId;
    UUID cpmId;
}
