package com.wh.gppmock.domain.ds;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wh.gppmock.domain.common.Jurisdiction;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonIgnoreProperties(value = "timestamp", allowGetters = true)
public class TransactionLog {

    UUID transactionId;
    LocalDateTime timestamp;
    UUID customerId;
    Jurisdiction jurisdiction;
    PamType pamType;
    String pamCustomerId;
    TransactionType transactionType;
    UUID cpmId;
    String currency;
    Integer amount;
    TransactionStatus status;
    List<String> logText;
    Map<String, String> data;
}
