package com.wh.gppmock.domain.ds;

public enum TransactionStatus {
    SUCCESS, FAILURE
}
