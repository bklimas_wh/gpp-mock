package com.wh.gppmock.domain.ds;

public enum TransactionType {
    DEPOSIT, WITHDRAWAL
}
