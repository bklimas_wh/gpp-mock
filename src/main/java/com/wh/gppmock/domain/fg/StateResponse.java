package com.wh.gppmock.domain.fg;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class StateResponse {
    String state;
}
