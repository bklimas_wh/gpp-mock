package com.wh.gppmock.domain.fg;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Value;

@Value
public class StateUpdate {
    Long newProcessInstanceId;
    String bundleType;
    Set<String> state;
    Map<String, Object> extendedState;

    @JsonCreator
    public StateUpdate(
        final Long newProcessInstanceId,
        final String bundleType,
        final Set<String> state,
        final Map<String, Object> extendedState
    ) {
        this.newProcessInstanceId = newProcessInstanceId;
        this.bundleType = bundleType;
        this.state = Collections.unmodifiableSet(state);
        this.extendedState = Collections.unmodifiableMap(extendedState);
    }
}
