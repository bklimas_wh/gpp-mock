package com.wh.gppmock.domain.hexopay;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Customer {
    String ip;
    String email;
    String firstName;
    String lastName;
    String middleName;
    String country;
    String city;
    String zip;
    String address;
    String phone;
    String deviceId;
}
