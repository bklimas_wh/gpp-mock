package com.wh.gppmock.domain.hexopay;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Request {
    Integer amount;
    String currency;
    String description;
    Boolean test;
    String expiredAt;
    String trackingId;
    String ip;
    String language;
    String notificationUrl;
    String returnUrl;
    Customer customer;
    Method method;
}
