package com.wh.gppmock.domain.hexopay;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Transaction {

    String status;
    String trackingId;

    String uid;
    String type;
    Integer amount;
    String currency;
    String description;
    String createdAt;
    String updatedAt;
    String methodType;
    Payment payment;
    MuchBetter muchBetter;
    Customer customer;
    String message;
    Boolean test;
    String language;
    BillingAddress billingAddress;
    AdditionalData additionalData;
    BeProtectedVerification beProtectedVerification;
}
