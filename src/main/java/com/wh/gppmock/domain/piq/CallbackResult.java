package com.wh.gppmock.domain.piq;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class CallbackResult implements Serializable {
    CallbackType callbackType;
    String userId;
    String sessionId;
    boolean success;
    String txId;
    int merchantTxId;
    UUID authCode;
    String userCat;
    String kycStatus;
    Sex sex;
    String firstName;
    String lastName;
    String street;
    String city;
    String state;
    String zip;
    String country;
    String email;
    String dob;
    String mobile;
    double balance;
    String balanceCy;
    String locale;
    Map<String, Object> attributes;
    int errCode;
    String errMsg;
}