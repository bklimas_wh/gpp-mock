package com.wh.gppmock.domain.piq;

public enum CallbackType {
    VERIFY_USER,
    AUTHORIZE,
    TRANSFER,
    CANCEL,
    ERROR
}