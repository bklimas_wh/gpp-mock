package com.wh.gppmock.domain.piq;

public enum Sex {
    MALE,
    FEMALE,
    UNKNOWN,
    X
}