package com.wh.gppmock.domain.pmb;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class AccountLimit implements Serializable {
    String accountId;
    String max;

    @Override
    public String toString() {
        return "\"accountId\":\"" + accountId + "\", \"max\":\"" + max + "\"";
    }
}
