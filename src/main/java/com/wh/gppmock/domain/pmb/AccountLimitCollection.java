package com.wh.gppmock.domain.pmb;

import java.io.Serializable;
import java.util.Collection;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class AccountLimitCollection implements Serializable {
    Collection<AccountLimit> accountLimits;
}
