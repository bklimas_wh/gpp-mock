package com.wh.gppmock.domain.pmb;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class MethodLimit implements Serializable {
    String methodName;
    String amount;
}
