package com.wh.gppmock.domain.pmb;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wh.gppmock.domain.common.Money;
import com.wh.gppmock.domain.ds.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;


@Value
@Builder(toBuilder = true)
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class PaymentMethodTransactionRequest implements Serializable {
    RequestMode requestMode;
    UUID transactionId;
    TransactionType transactionType;
    UUID cpmId;
    UUID customerId;
    Money amount;
}