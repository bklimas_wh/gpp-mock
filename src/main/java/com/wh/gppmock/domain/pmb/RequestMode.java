package com.wh.gppmock.domain.pmb;

public enum RequestMode {
    ADD, REMOVE
}
