package com.wh.gppmock.domain.pmb;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wh.gppmock.domain.common.Money;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class ValidationRequest implements Serializable {
    UUID cpmId;
    UUID customerId;
    Money amount;
    Integer accountBalance;
    String universe;
}