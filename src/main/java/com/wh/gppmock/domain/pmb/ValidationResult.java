package com.wh.gppmock.domain.pmb;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class ValidationResult implements Serializable {

    public static final ValidationResult SUCCESS = ValidationResult.builder().valid(true).build();

    boolean valid;
    String failureReason;
}