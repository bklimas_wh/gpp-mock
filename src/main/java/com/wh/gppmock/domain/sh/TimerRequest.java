package com.wh.gppmock.domain.sh;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class TimerRequest implements Serializable {
    String containerId;
    String processId;
    String signalName;
    String subType;
}
