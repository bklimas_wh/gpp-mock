package com.wh.gppmock.domain.un;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wh.gppmock.domain.common.Money;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class AuthoriseUnityRequest implements Serializable {
    String accessToken;
    String universe;
    String accountId;
    String paymentReference;
    Money amount;
    String type;
    Provider provider;

    String externalTransactionId;
    String externalAccountId;

    Map<String, String> attributes;
}
