package com.wh.gppmock.domain.un;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wh.gppmock.domain.common.Money;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.With;

@Value
@Builder
@With
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class Customer implements Serializable {
    String id;
    UUID customerId;
    String email;
    String nationalId;
    String username;
    String firstName;
    String lastName;
    String street;
    String houseNumber;
    String city;
    String postCode;
    String country;
    String currency;
    String countryCallCode;
    String mobileNumber;
    LocalDate dateOfBirth;
    String status;
    Integer incorrectPasswordAttempts;
    String registrationIp;
    String selectedLanguage;
    String oddsFormat;
    String geolocationCountry;
    String userAgent;
    String registrationChannel;
    String affiliateId;
    Boolean emailCommunication;
    Boolean smsCommunication;
    Permissions permissions;
    String placeOfBirth;
    String nationality;
    String state;
    String maidenName;
    String termsAndConditionsVersion;
    Boolean privacyPolicy;
    Boolean isTestAccount;
    String lastLoginIp;
    LocalDateTime lastLoginDatetime;
    String gender;
    String timezone;
    LocalDateTime registrationDatetime;
    String kycVerificationStatus;
    String blockReason;
    String closeReason;
    Money balance;
}
