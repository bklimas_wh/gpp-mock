package com.wh.gppmock.domain.un;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class FailedCondition implements Serializable {
    String name;
    String description;
    Object details;
    List<String> denied;
}
