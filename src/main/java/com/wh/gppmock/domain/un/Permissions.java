package com.wh.gppmock.domain.un;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class Permissions implements Serializable {
    List<String> granted;
    List<FailedCondition> failedConditions;
}
