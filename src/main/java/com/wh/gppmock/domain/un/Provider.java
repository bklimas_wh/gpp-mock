package com.wh.gppmock.domain.un;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wh.gppmock.domain.common.Money;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class Provider implements Serializable {
    String name;
    String service;
    Integer expectedFee;
    Money providerAmount;
    String providerReference;
    Integer fee;
    String feeMode;
}
