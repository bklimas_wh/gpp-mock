package com.wh.gppmock.domain.un;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class StartUnityRequest implements Serializable {
    String accessToken;
    String universe;
    String accountId;
    String paymentReference;
}
