package com.wh.gppmock.domain.un;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.With;

@Value
@Builder
@With
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class StartUnityResponse implements Serializable {
    String paymentReference;
    Integer minAmount;
    Integer maxAmount;
    Customer customer;
}
