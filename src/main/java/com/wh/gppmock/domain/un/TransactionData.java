package com.wh.gppmock.domain.un;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class TransactionData implements Serializable {
    String id;
    Integer amount;
    String currency;
    LocalDateTime createdAt;
    Integer runningBalance;
    Integer freeAmount;
    TransactionDetails transactionDetails;

    String description;
    String category;
}
