package com.wh.gppmock.domain.un;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.wh.gppmock.domain.common.Money;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor(onConstructor = @__(@JsonCreator))
public class UnityResponse implements Serializable {

    List<TransactionData> transactionsData;
    Money balance;
    Money cashBalance;
    Object limits;
}
