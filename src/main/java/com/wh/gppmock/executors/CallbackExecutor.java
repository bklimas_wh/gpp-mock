package com.wh.gppmock.executors;

import java.time.Duration;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.wh.gppmock.domain.hexopay.DepositCallback;
import com.wh.gppmock.domain.hexopay.Payment;
import com.wh.gppmock.domain.hexopay.PaymentRequest;
import com.wh.gppmock.domain.hexopay.PaymentResponse;
import com.wh.gppmock.domain.hexopay.Transaction;
import com.wh.gppmock.service.Sleeper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CallbackExecutor {
    private final RestTemplate restTemplate;
    private final Sleeper sleeper;
    private final ThreadPoolExecutor executor;

    @Value("${callback.delayTime.min}")
    private Integer minDelayTime;

    @Value("${callback.delayTime.max}")
    private Integer maxDelayTime;

    @Value("${callback.retry}")
    private Integer retry;

    public CallbackExecutor(final RestTemplateBuilder restTemplateBuilder, final Sleeper sleeper) {
        this.restTemplate = restTemplateBuilder.build();
        this.sleeper = sleeper;
        executor = new ThreadPoolExecutor(64, 128, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
    }

    public void perform(PaymentRequest paymentRequest, PaymentResponse paymentResponse) {
        executor.submit(() -> callback(paymentRequest, paymentResponse));
        log.info("Active thread in pool {}", executor.getActiveCount());
    }

    private void callback(PaymentRequest paymentRequest, PaymentResponse paymentResponse) {
        var depositCallback = createDepositCallback(paymentRequest, paymentResponse);

        var notificationUrl = paymentRequest.getRequest().getNotificationUrl();
        var trackingId = paymentRequest.getRequest().getTrackingId();
        for (int i = 0; i < retry; i++) {
            if (executeCallbackRequest(notificationUrl, trackingId, depositCallback)) {
                break;
            }
        }

    }

    private boolean executeCallbackRequest(String notificationUrl, String trackingId, DepositCallback depositCallback) {
        sleeper.sleep(minDelayTime, maxDelayTime);
        log.info("Perform callback request: {}", trackingId);

        restTemplate.postForEntity(notificationUrl, depositCallback, Void.class);
        return true;
    }

    private DepositCallback createDepositCallback(PaymentRequest paymentRequest, PaymentResponse paymentResponse) {
        var responseTransaction = paymentResponse.getTransaction();
        var successful = responseTransaction.getAmount() % 2 == 0;
        var message = successful ? "Transaction was successfully processed" :
            "The transaction was failed; This is a very bad transaction :)";
        var status = successful ? "successful" : "failed";

        var transaction = Transaction.builder()
            .uid(responseTransaction.getUid())
            .trackingId(responseTransaction.getTrackingId())
            .type(responseTransaction.getType())
            .status(status)
            .amount(responseTransaction.getAmount())
            .currency(responseTransaction.getCurrency())
            .description(responseTransaction.getDescription())
            .createdAt(responseTransaction.getCreatedAt())
            .updatedAt(responseTransaction.getUpdatedAt())
            .methodType(responseTransaction.getMethodType())
            .payment(Payment.builder()
                .status(status)
                .message(message)
                .gatewayId(responseTransaction.getPayment().getGatewayId())
                .refId(responseTransaction.getPayment().getRefId())
                .build())
            .message(message)
            .build();

        return DepositCallback.builder()
            .transaction(transaction)
            .build();
    }
}


/*
{
  "transaction": {
    "uid": "783d284f-e815-4d42-afad-9221c56afade",
    "tracking_id": "9d95126f-5f57-4496-a892-e453f37ad44c",
    "type": "payment",
    "status": "successful",
    "amount": 200,
    "currency": "EUR",
    "description": "WH Transaction reference: 945180",
    "created_at": "2020-09-23T11:32:58Z",
    "updated_at": "2020-09-23T11:34:36Z",
    "method_type": "much_better",
    "payment": {
      "status": "failed",
      "gateway_id": 15605,
      "ref_id": 31762018,
      "message": "Transaction was successfully processed"
    },
    "method_name": {
      "type": "much_better",
      "account": "48698657087",
      "channel": "1|Logo Text",
      "customerStatus": "registered"
    },
    "customer": {},
    "message": "Transaction was successfully processed",
    "test": false,
    "language": null,
    "billing_address": {
    },
    "additional_data": {
    }
  }
}
 */

/*
{
  "transaction": {
    "uid": "783d284f-e815-4d42-afad-9221c56afade",
    "tracking_id": "8b7d80f2-6d62-4dda-a594-71d3a2672108",
    "type": "payment",
    "status": "failed",
    "amount": 200,
    "currency": "EUR",
    "description": "WH Transaction reference: 945180",
    "created_at": "2020-09-23T11:32:58Z",
    "updated_at": "2020-09-23T11:34:36Z",
    "method_type": "much_better",
    "payment": {
      "status": "failed",
      "gateway_id": 15605,
      "ref_id": 31762018,
      "message": "Transaction was failed"
    },
    "method_name": {
      "type": "much_better",
      "account": "48698657087",
      "channel": "1|Logo Text",
      "customerStatus": "registered"
    },
    "customer": {},
    "message": "The transaction was failed; This is a very bad transaction :)",
    "test": false,
    "language": null,
    "billing_address": {
    },
    "additional_data": {
    }
  }
}
 */