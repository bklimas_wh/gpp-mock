package com.wh.gppmock.measure;

import java.util.concurrent.atomic.AtomicInteger;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TPSMeasure {
    private final String name;
    private final AtomicInteger counter = new AtomicInteger(0);
    private final AtomicInteger max = new AtomicInteger(0);
    private final AtomicInteger all = new AtomicInteger(0);

    public TPSMeasure(String name) {
        this.name = name;
    }

    public void printAndClear() {
        max.getAndSet(Math.max(counter.get(), max.get()));
        log.info("{} current TPS {} max {} all requests {}", name, counter.get() / 10.0, max.get() / 10.0, all.get());
        counter.set(0);
    }

    public void increment() {
        counter.incrementAndGet();
        all.incrementAndGet();
    }
}
