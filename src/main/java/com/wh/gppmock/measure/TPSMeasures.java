package com.wh.gppmock.measure;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wh.gppmock.measure.annotation.Measure;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

@Slf4j
@Aspect
@Component
public class TPSMeasures {
    private final Map<String, TPSMeasure> measures = new ConcurrentHashMap<>();

    @Pointcut("@annotation(com.wh.gppmock.measure.annotation.Measure)")
    private void measuredMethods() {}

    @Before("measuredMethods()")
    public void measure(final JoinPoint jp) {
        var measureName = ((MethodSignature)jp.getSignature())
            .getMethod()
            .getAnnotation(Measure.class)
            .value();
        measures.computeIfAbsent(measureName, TPSMeasure::new).increment();
    }

    @Scheduled(fixedRate = 10_000)
    public void print() {
        log.info("================================================");
        var sorted = new TreeMap<>(measures);
        sorted.forEach((name, measure) -> measure.printAndClear());
        log.info("================================================");
    }
}
