package com.wh.gppmock.service;

import org.springframework.stereotype.Service;

import com.wh.gppmock.domain.ds.TransactionLog;
import com.wh.gppmock.measure.annotation.Measure;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class DataStoreService {
    private final Sleeper sleeper;

    @Measure("DS-LOG")
    public void save(final TransactionLog transactionLog) {
        log.info("DS-TL: save transaction log {} {} {}", transactionLog.getTransactionId(),
            transactionLog.getTransactionType(), transactionLog.getLogText());
        sleeper.sleep();
    }
}
