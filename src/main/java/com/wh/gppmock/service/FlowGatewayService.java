package com.wh.gppmock.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.wh.gppmock.domain.fg.StateUpdate;
import com.wh.gppmock.measure.annotation.Measure;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class FlowGatewayService {
    private final Sleeper sleeper;

    private final Map<Long, LinkedList<State>> states = new ConcurrentHashMap<>();

    @Measure("FG-UPD")
    public void update(final long processInstanceId, final StateUpdate body) {
        var status = body.getState().iterator().next();
        log.info("FG-UP: Update state for {} {}", processInstanceId, status);
        var state = states.getOrDefault(processInstanceId, new LinkedList<>());
        var version = state.size();
        state.add(State.builder().version(version).status(status).build());
        states.put(processInstanceId, state);
        sleeper.sleep();
    }

    @Measure("FG-GET")
    public String getState(final Long processInstanceId, final int version) {
        log.info("FG-GT: Get state for {} {}", processInstanceId, version);
        int i = 0;
        do {
            var state = states.get(processInstanceId);
            if (state != null && state.getLast() != null && state.getLast().getVersion() == version) {
                return state.getLast().getStatus();
            }
            sleeper.sleep();
        } while (i++ < 30);
        log.warn("FG-GT: Get state for {} {} return old one", processInstanceId, version);
        return Optional.ofNullable(states.get(processInstanceId))
            .map(LinkedList::getLast)
            .map(State::getStatus)
            .orElse(null);
    }

    @Builder
    @Getter
    private static final class State {
        private int version;
        private String status;
    }
}

//2022-03-01 21:32:09.604  INFO 79756 --- [nio-8100-exec-2] c.wh.gppmock.service.FlowGatewayService  : FG-GT: Get state for 366 1
//2022-03-01 21:32:20.164  WARN 79756 --- [nio-8100-exec-2] c.wh.gppmock.service.FlowGatewayService  : FG-GT: Get state for 366 1 return old one