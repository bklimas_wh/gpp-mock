package com.wh.gppmock.service;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.wh.gppmock.domain.pmb.AccountLimit;
import com.wh.gppmock.domain.pmb.AccountLimitCollection;
import com.wh.gppmock.domain.pmb.MethodLimit;
import com.wh.gppmock.domain.pmb.PaymentMethodTransactionRequest;
import com.wh.gppmock.domain.pmb.ValidationRequest;
import com.wh.gppmock.domain.pmb.ValidationResult;
import com.wh.gppmock.measure.annotation.Measure;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class PMBService {
    private Sleeper sleeper;

    @Measure("PMB-ML")
    public Collection<MethodLimit> getPaymentMethodBalanceLimits(UUID customerId) {
        log.info("PMB-ML Get PMB limit for {}", customerId);
        sleeper.sleep();
        return List.of(MethodLimit.builder()
            .amount("€50")
            .methodName("Credit Card - 521002******3560")
            .build());
    }

    @Measure("PMB-MT")
    public void processPaymentMethodTransaction(final PaymentMethodTransactionRequest transactionWithMode) {
        log.info("PMB-MT Add payment method transaction {}", transactionWithMode);
        sleeper.sleep();
    }

    @Measure("PMB-AL")
    public AccountLimitCollection getAccountLimits(final UUID customerId) {
        log.info("PMB-AL Get account limit for customerId {}", customerId);
        sleeper.sleep();

        var accountLimits = List.of(AccountLimit.builder()
            .accountId(UUID.randomUUID().toString())
            .max("20")
            .build());
        return new AccountLimitCollection(accountLimits);
    }

    @Measure("PMB-VL")
    public ValidationResult validateAgainstPaymentMethodBalance(final ValidationRequest request) {
        log.info("PMB-VAL Validate payment method balance {}", request);
        sleeper.sleep();
        return ValidationResult.SUCCESS;
    }
}
