package com.wh.gppmock.service;

import org.springframework.stereotype.Service;

import com.wh.gppmock.domain.piq.CallbackResult;
import com.wh.gppmock.measure.annotation.Measure;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class PaymentIqService {
    private final Sleeper sleeper;

    @Measure("PIQ-PC")
    public void processCallbackResult(final String callbackId, final CallbackResult callbackResult) {
        log.info("PIQ-P Process callback result {} {}", callbackId, callbackResult);
        sleeper.sleep();
    }
}
