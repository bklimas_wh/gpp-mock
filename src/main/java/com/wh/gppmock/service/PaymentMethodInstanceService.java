package com.wh.gppmock.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.wh.gppmock.domain.ds.PaymentMethodInstance;
import com.wh.gppmock.domain.ds.PaymentMethodInstanceCollection;
import com.wh.gppmock.domain.ds.PaymentMethodStatus;
import com.wh.gppmock.domain.ds.PaymentMethodType;
import com.wh.gppmock.measure.annotation.Measure;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class PaymentMethodInstanceService {
    private final Sleeper sleeper;

    @Measure("PMI-FD")
    public PaymentMethodInstanceCollection findByAttributes(final Map<String, String> attributes) {
        log.info("PMI-F: find pmi by attributes {} ", attributes);
        var paymentMethodInstance = PaymentMethodInstance.builder()
            .cpmId(UUID.randomUUID())
            .customerId(UUID.fromString(attributes.get("customerId")))
            .status(PaymentMethodStatus.ENABLED)
            .type(PaymentMethodType.valueOf(attributes.get("type")))
            .attributes(Map.of("piqAccountId", attributes.get("piqAccountId")))
            .build();
        sleeper.sleep();
        return new PaymentMethodInstanceCollection(0, 0, List.of(paymentMethodInstance));
    }
}
