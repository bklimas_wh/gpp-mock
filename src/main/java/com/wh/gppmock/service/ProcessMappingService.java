package com.wh.gppmock.service;

import org.springframework.stereotype.Service;

import com.wh.gppmock.measure.annotation.Measure;
import com.wh.gppmock.domain.ds.ProcessMappingRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class ProcessMappingService {
    private final Sleeper sleeper;

    @Measure("DS-CPM")
    public Long create(final ProcessMappingRequest processMapping) {
        log.info("DS-PM: create process mapping {}", processMapping);
        sleeper.sleep();
        return processMapping.getProcessInstanceId();
    }
}
