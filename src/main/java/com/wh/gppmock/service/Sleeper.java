package com.wh.gppmock.service;

import java.util.Random;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class Sleeper {
    private static final int MIN_SLEEP = 200;
    private static final int MAX_SLEEP = 500;
    private final Random random = new Random();

    public void sleep() {
        sleep(MIN_SLEEP, MAX_SLEEP);
    }

    public void sleep(final int min, final int max) {
        var sleep = random.ints(min, max).findFirst().getAsInt();
        sleep(sleep);
    }

    public void sleep(final int sleep) {
        try {
            log.info("Performing sleep {} ms", sleep);
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            log.warn("Error during sleep {}", e.getMessage());
        }
    }
}
