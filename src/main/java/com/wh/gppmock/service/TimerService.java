package com.wh.gppmock.service;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.wh.gppmock.measure.annotation.Measure;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class TimerService {
    private Sleeper sleeper;

    @Measure("SH-ADD")
    public void addTimer(final String containerId, final String processId, final String signalName,
                         final String subType) {
        log.info("SH-AD Creating timer for {}, {}, {} and {}", containerId, processId, signalName, subType);
        sleeper.sleep();
    }

    @Measure("SH-DEL")
    public void deleteTimer(final String containerId, final String processId, final String signalName) {
        log.info("SH-DL Deleting timer for {} and {}", containerId, processId);
        sleeper.sleep();
    }
}
