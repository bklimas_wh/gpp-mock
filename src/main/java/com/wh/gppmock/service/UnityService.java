package com.wh.gppmock.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.wh.gppmock.domain.common.Currency;
import com.wh.gppmock.domain.common.Money;
import com.wh.gppmock.domain.un.AuthoriseUnityRequest;
import com.wh.gppmock.domain.un.CompleteUnityRequest;
import com.wh.gppmock.domain.un.Customer;
import com.wh.gppmock.domain.un.StartUnityRequest;
import com.wh.gppmock.domain.un.StartUnityResponse;
import com.wh.gppmock.domain.un.TransactionData;
import com.wh.gppmock.domain.un.TransactionDetails;
import com.wh.gppmock.domain.un.UnityResponse;
import com.wh.gppmock.measure.annotation.Measure;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class UnityService {
    private final Sleeper sleeper;

    @Measure("UN-SDP")
    public StartUnityResponse startDeposit(final StartUnityRequest startUnityRequest) {
        log.info("UN-SDP: start deposit {}", startUnityRequest);
        sleeper.sleep();

        return getStartUnityResponse();
    }

    @Measure("UN-SWD")
    public StartUnityResponse startWithdrawal(final StartUnityRequest startUnityRequest) {
        log.info("UN-SWD: start withdrawal {}", startUnityRequest);
        sleeper.sleep();

        return getStartUnityResponse();
    }

    @Measure("UN-ADP")
    public void authoriseDeposit(final AuthoriseUnityRequest authoriseUnityRequest) {
        log.info("UN-ADP: authorise deposit {}", authoriseUnityRequest);
        sleeper.sleep();
    }

    @Measure("UN-AWD")
    public UnityResponse authoriseWithdrawal(final AuthoriseUnityRequest authoriseUnityRequest) {
        log.info("UN-ADP: authorise withdrawal {}", authoriseUnityRequest);
        sleeper.sleep();

        return getUnityResponse();
    }

    @Measure("UN-CDP")
    public void completeDeposit(final CompleteUnityRequest completeUnityRequest) {
        log.info("UN-CDP: complete deposit {}", completeUnityRequest);
        sleeper.sleep();
    }

    @Measure("UN-CWD")
    public UnityResponse completeWithdrawal(final CompleteUnityRequest completeUnityRequest) {
        log.info("UN-CWD: complete withdrawal {}", completeUnityRequest);
        sleeper.sleep();
        return getUnityResponse();
    }

    private UnityResponse getUnityResponse() {
        return UnityResponse.builder()
            .balance(Money.builder().value(100000).currency(Currency.EUR).build())
            .cashBalance(Money.builder().value(100000).currency(Currency.EUR).build())
            .transactionsData(List.of(TransactionData.builder()
                    .amount(-3000)
                    .currency("EUR")
                    .createdAt(LocalDateTime.of(2022, 2, 15, 8, 15,16))
                    .description("Amount Withdrawal transaction.")
                    .freeAmount(0)
                    .id(UUID.randomUUID().toString())
                    .runningBalance(100000)
                    .transactionDetails(TransactionDetails.builder()
                        .action("Withdrawal")
                        .transactionGroup(UUID.randomUUID().toString())
                        .transactionType("PaymentData")
                        .build())
                .build()))
            .build();
    }

    private StartUnityResponse getStartUnityResponse() {
        return StartUnityResponse.builder()
            .paymentReference("61b7495d-649c-472f-a8ef-02959aec6c4f")
            .minAmount(1000)
            .maxAmount(1000000000)
            .customer(Customer.builder()
                .id("U000001XJT")
                .customerId(UUID.fromString("00242581-094b-435d-8357-dd417bece2c6"))
                .nationalId("1234567890")
                .email("gpp_test_bkl@test.com")
                .firstName("Delfina")
                .lastName("Okuneva")
                .street("Rohan Cliff")
                .houseNumber("937")
                .city("South Celestinoport")
                .postCode("01022")
                .country("DE")
                .currency("EUR")
                .countryCallCode("+45")
                .mobileNumber("12742372618")
                .dateOfBirth(LocalDate.of(1987,4,7))
                .status("active")
                .incorrectPasswordAttempts(0)
                .registrationIp("100.75.101.217")
                .selectedLanguage("da-DE")
                .oddsFormat("decimal")
                .geolocationCountry("DE")
                .userAgent("PostmanRuntime/7.26.8")
                .registrationChannel("desktop")
                .emailCommunication(false)
                .smsCommunication(false)
                .maidenName("Sam")
                .placeOfBirth("Chile")
                .nationality("DE")
                .state("Hamburg")
                .termsAndConditionsVersion("1.0")
                .lastLoginIp("100.75.98.130")
                .lastLoginDatetime(LocalDateTime.of(2022, 2, 10, 17, 0, 19))
                .gender("male")
                .timezone("Europe/London")
                .registrationDatetime(LocalDateTime.of(2021, 2, 10, 17, 0, 19))
                .kycVerificationStatus("successful")
                .balance(Money.builder()
                    .value(88550)
                    .currency(Currency.EUR)
                    .build())
                .build())
            .build();
    }
}


